﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinWarsztatClient.Views
{
    /// <summary>
    /// Klasa pomocnicza dla pozycji w "hamburger" menu
    /// </summary>
    public class MasterPageItem
    {
        public string Title { get; set; }
        public string Icon { get; set; }
        public Type PageType { get; set; }
    }
}
