﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XamarinWarsztatClient.Models;

namespace XamarinWarsztatClient.ViewModels
{
    public class VMMyCars : INotifyPropertyChanged
    {
        private ModelCars _oldCar;
        private ObservableCollection<ModelCars> _myCars;

        public VMMyCars()
        {
            getAllClientCars();
        }

        public ObservableCollection<ModelCars> MyCars
        {
            get { return _myCars; }
            set
            {
                _myCars = value;
                OnPropertyChanged("MyCars");
            }
        }

        public ModelCars activeCar { get; }

        public async Task RefreshCarsList()
        {
            MyCars.Clear();
            MyCars = new ObservableCollection<ModelCars>();
            List<ModelCars> carsList = await DataBase.DataBase.GetAllUserCars(Context.UserId);
            MyCars = new ObservableCollection<ModelCars>(carsList);
        }
        
        /// <summary>
        /// Metoda pobierająca i ustawiająca wszystkie pojazdy użytkownika do listy
        /// </summary>
        /// <returns></returns>
        private async Task getAllClientCars()
        {
            List<ModelCars> carsList = await DataBase.DataBase.GetAllUserCars(Context.UserId);
            MyCars = new ObservableCollection<ModelCars>(carsList);
        }


        private void updateMyCars(ModelCars car)
        {
            var index = MyCars.IndexOf(MyCars.Where(x => x.Id == car.Id).First());
            MyCars.Remove(car);
            MyCars.Insert(index, car);
        }

        /// <summary>
        /// Metoda usuwająca pojazd użytkownika
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteCar()
        {
            _oldCar.PoCzyWidoczny = Constants.carNotVisible;
            if (await DataBase.DataBase.SaveCar(_oldCar))
            {
                _oldCar = null;
                await RefreshCarsList();
                return true;   
            }
            else
            {
                return false;
            }
        }

        public void HideOrShowCar(ModelCars car)
        {
            if (_oldCar != null && _oldCar.Id == car.Id)
            {
                car.IsVisible = !car.IsVisible;
                updateMyCars(car);
            }
            else
            {
                if (_oldCar != null)
                {
                    _oldCar.IsVisible = false;
                    updateMyCars(_oldCar);
                }

                car.IsVisible = true;
                updateMyCars(car);
            }

            _oldCar = car;
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
