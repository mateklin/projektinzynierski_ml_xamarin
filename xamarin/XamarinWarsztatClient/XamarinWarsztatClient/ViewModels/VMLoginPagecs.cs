﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XamarinWarsztatClient.Models;

namespace XamarinWarsztatClient.ViewModels
{
    public class VMLoginPage : ModelUser
    {
        public bool HasValidInput { get { return !String.IsNullOrEmpty(Login) && !String.IsNullOrEmpty(Password); } }

        public bool RHasValidInput
        {
            get
            {
                return
                    !String.IsNullOrEmpty(RLogin) && !String.IsNullOrEmpty(RPassword) && !String.IsNullOrEmpty(RPesel) && !String.IsNullOrEmpty(RName) && !String.IsNullOrEmpty(RPassword)
                    && !String.IsNullOrEmpty(RSurname) && !String.IsNullOrEmpty(RRepassword)
                && RPassword.Length > 4 && RLogin.Length > 4 && RPesel.Length > 4 && RName.Length > 4 && RSurname.Length > 4 && RPassword == RRepassword;
            }
        }

    }
}
