﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XamarinWarsztatClient.Models;

namespace XamarinWarsztatClient.ViewModels
{
    public class VMMyVisits : INotifyPropertyChanged
    {
        private ModelVisit _oldVisit;
        private ObservableCollection<ModelVisit> _myVisits;

        public VMMyVisits()
        {
            
        }

        public ObservableCollection<ModelVisit> MyVisits
        {
            get { return _myVisits; }
            set
            {
                _myVisits = value;
                OnPropertyChanged("MyVisits");
                
            }
        }

        public void HideOrShowVisit(ModelVisit visit)
        {
            if(_oldVisit == visit)
            {
                visit.IsVisible = !visit.IsVisible;
                updateMyVisit(visit);
            }
            else
            {
                if(_oldVisit != null)
                {
                    _oldVisit.IsVisible = false;
                    updateMyVisit(_oldVisit);
                }

                visit.IsVisible = true;
                updateMyVisit(visit);
            }

            _oldVisit = visit;
        }


        /// <summary>
        /// Metoda pobierająca i ustawiająca wszystkie pojazdy użytkownika do listy
        /// </summary>
        /// <returns></returns>
        public async Task getAllClientVisits(bool archives = false)
        {
            List<ModelVisit> listVisit = await DataBase.DataBase.GetAllVisit(Context.UserId, archives);
            MyVisits = new ObservableCollection<ModelVisit>(listVisit);
        }

        private void updateMyVisit(ModelVisit visit)
        {
            var index = MyVisits.IndexOf(visit);
            MyVisits.Remove(visit);
            MyVisits.Insert(index, visit);
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
