﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XamarinWarsztatClient.Models;

namespace XamarinWarsztatClient.ViewModels
{
    public class VMServices : INotifyPropertyChanged
    {
        private ModelService _oldService;
        private ObservableCollection<ModelService> _services;

        public ObservableCollection<ModelService> Services
        {
            get
            {
                return _services;
            }
            set
            {
                _services = value;
                OnPropertyChanged("Services");
            }
        }

        private void updateMyService(ModelService service)
        {
            var index = Services.IndexOf(service);
            Services.Remove(service);
            Services.Insert(index, service);
        }

        public async Task GetAllVisitServices(long visitId)
        {
            int visitInt = Convert.ToInt32(visitId);
            Services = new ObservableCollection<ModelService>(await DataBase.DataBase.GetAllServiceList(visitInt));
        }

        public void HideOrShowService(ModelService service)
        {
            if(service == _oldService)
            {
                service.IsVisible = !service.IsVisible;
                updateMyService(service);
            }
            else
            {
                if(_oldService != null)
                {
                    _oldService.IsVisible = false;
                    updateMyService(_oldService);
                }

                service.IsVisible = true;
                updateMyService(service);
            }

            _oldService = service;
        }

 

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
