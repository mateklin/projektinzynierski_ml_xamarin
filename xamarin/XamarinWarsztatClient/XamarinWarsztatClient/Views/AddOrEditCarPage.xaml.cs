﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinWarsztatClient.Models;

namespace XamarinWarsztatClient.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddOrEditCarPage : ContentPage
    {
        private ModelCars _car;
        private MyCarsPage _parentPage;

        private List<BrandCars> _brands;
        private List<ModelsForCars> _models;

        public AddOrEditCarPage(ModelCars car, MyCarsPage carsPage)
        {
            InitializeComponent();
            _parentPage = carsPage;

            this.busyView.IsVisible = true;

            setControlsEnabled(false);
            _car = car;
           
            AfterInitAsync();

        }

        /// <summary>
        /// Ustawienie dostępności kontrolek
        /// </summary>
        /// <param name="value"></param>
        private void setControlsEnabled(bool value)
        {
            ModelPicker.IsEnabled = value;
            BrandPicker.IsEnabled = value;
            eVin.IsEnabled = value;
            eLicensePlate.IsEnabled = value;
            
        }

        /// <summary>
        /// Metoda wywoływana po konstruktorze
        /// </summary>
        /// <returns></returns>
        private async Task AfterInitAsync()
        {
            _brands = await DataBase.DataBase.GetAllBrands();
            _models = await DataBase.DataBase.GetAllModels();
            
            foreach(var item in _brands)
            {
                BrandPicker.Items.Add(item.MNazwa);
            }

            if (_car.Id != 0)
            {
                eVin.Text = _car.PoVin;
                eLicensePlate.Text = _car.PoNrRej;

                BrandPicker.SelectedIndex = Convert.ToInt32(_car.PoMarka - 1);
                var idModel = _models.Where(x => x.Id == _car.PoModel).First();
                for (int i = 0; i < ModelPicker.Items.Count; i++)                              
                {
                    if(ModelPicker.Items[i] == idModel.MoNazwa)
                    {
                        ModelPicker.SelectedIndex = i;
                        break;
                    }
                }
            }

            this.busyView.IsVisible = false;

            setControlsEnabled(true);
        }

        /// <summary>
        /// Ustawianie pickera z modelami na podstawie id marki
        /// </summary>
        /// <param name="brandID"></param>
        private void setCBModels(int brandID)
        {
            ModelPicker.Items.Clear();

            var models = _models.Where(x => x.IdMarki == brandID);

            foreach (var item in models)
            {
                ModelPicker.Items.Add(item.MoNazwa);
            }
        }

        private void BrandPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = sender as Picker;

            int brandID = picker.SelectedIndex + 1;

            setCBModels(brandID);
        }

        /// <summary>
        /// Zdarzenie wywoływane po wciśniećiu "Zapisz pojazd".
        /// Zapisuje bądź uaktualnia dane o pojeździe klienta.
        /// </summary>
        private async Task ToolbarItem_Clicked(object sender, EventArgs e)
        {
            if (ModelPicker.SelectedIndex == -1 || BrandPicker.SelectedIndex == -1 || string.IsNullOrWhiteSpace(eVin.Text) || string.IsNullOrWhiteSpace(eLicensePlate.Text))
                await DisplayAlert("Błąd", "Formularz nie jest całkowicie wypełniony", "Ok");
            else
            {
                _car.PoMarka = BrandPicker.SelectedIndex + 1;
                _car.PoModel = _models.Where(x => x.IdMarki == _car.PoMarka && x.MoNazwa == ModelPicker.Items[ModelPicker.SelectedIndex]).Select(x => x.Id).First();
                _car.PoVin = eVin.Text;
                _car.PoNrRej = eLicensePlate.Text;
                _car.PoCzyWidoczny = 1;
                _car.IdKlient = Context.UserId;


                if (await DataBase.DataBase.SaveCar(_car))
                    await DisplayAlert("Sukces", "Zapisano pojazd", "Ok");
                else
                    await DisplayAlert("Błąd", "Nie można zapisać pojazdu", "Ok");

                _parentPage.RefreshListView();

                await Navigation.PopAsync();

            }
        }
    }
}