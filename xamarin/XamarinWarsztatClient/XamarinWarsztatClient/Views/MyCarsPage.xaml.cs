﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinWarsztatClient.Models;
using XamarinWarsztatClient.ViewModels;

namespace XamarinWarsztatClient.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MyCarsPage : ContentPage
	{
        private ModelCars _activeCar;
		public MyCarsPage ()
		{
			InitializeComponent ();
		}

        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var vm = BindingContext as VMMyCars;
            var car = e.Item as ModelCars;
            _activeCar = car;

            vm.HideOrShowCar(car);
        }

        private async void ToolbarItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AddOrEditCarPage(new ModelCars(), this));
        }

        public void RefreshListView()
        {
            var vm = BindingContext as VMMyCars;

            vm.RefreshCarsList();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AddOrEditCarPage(_activeCar, this));
        }

        private async void Button_Clicked_1(object sender, EventArgs e)
        {
            var vm = BindingContext as VMMyCars;

            if (await DisplayAlert("Usuwanie pojazdu", "Czy na pewno chcesz usunać pojazd?", "Tak", "Nie"))
            {
                if(await vm.DeleteCar())
                {
                    await DisplayAlert("Sukces", "Samochód usunięto poprawnie.", "Ok");
                }
                else
                {
                    await DisplayAlert("Błąd", "Błąd z usuwaniem pojazdu.\nSpróbuj ponownie później", "Ok");
                }
            }
        }

    }
}