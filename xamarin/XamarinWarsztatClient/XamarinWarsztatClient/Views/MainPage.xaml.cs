﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinWarsztatClient.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : MasterDetailPage
    {
        public MainPage()
        {
            InitializeComponent();
            masterPage.ListView.ItemSelected += ListView_ItemSelected;

            if(Device.RuntimePlatform == Device.UWP)
            {
                MasterBehavior = MasterBehavior.Popover;
            }
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MasterPageItem;
            if (item != null)
            {
                if(item.Title == "Archiwum wizyt" || item.Title == "Aktualne wizyty")
                {
                    bool archivesItem = item.Title == "Archiwum wizyt";
                    Detail = new NavigationPage((Page)Activator.CreateInstance(item.PageType, archivesItem));
                }
                else
                    Detail = new NavigationPage((Page)Activator.CreateInstance(item.PageType));

                masterPage.ListView.SelectedItem = null;
                IsPresented = false;
            }
                

          
        }
    }
}