﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinWarsztatClient.Models;
using XamarinWarsztatClient.ViewModels;

namespace XamarinWarsztatClient.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VisitPage : ContentPage
    {
        private ModelVisit _visit;
        //model czynności
        public VisitPage(ModelVisit visit)
        {
            InitializeComponent();
            _visit = visit;

            afterInit();
        }

        private async Task afterInit()
        {
            var vm = BindingContext as VMServices;
            await vm.GetAllVisitServices(_visit.Id);

            lCost.Text = _visit.WCena;
            lDateAdopt.Text = _visit.WDataPrzy;

            if (!string.IsNullOrEmpty(_visit.WDataOdd))
            {
                SLDateGivingBack.IsVisible = true;
                lDateGivingBack.Text = _visit.WDataOdd;
            }
        }

        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var vm = BindingContext as VMServices;
            var service = e.Item as ModelService;

            vm.HideOrShowService(service);
        }
    }
}