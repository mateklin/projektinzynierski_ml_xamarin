﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinWarsztatClient.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PasswordChangePage : ContentPage
    {
        public PasswordChangePage()
        {
            InitializeComponent();
        }

        private async void ToolbarItem_Clicked(object sender, EventArgs e)
        {
            if (await DataBase.DataBase.IsPasswordCorrect(eOldPass.Text, Context.UserId))
            {
                if ((eNewPass.Text == eRetypePass.Text) && eNewPass.Text.Length > 0)
                {
                    if (await DataBase.DataBase.ChangeUserPassword(eNewPass.Text, Context.UserId))
                    {
                        await DisplayAlert("Sukces", "Hasło zostało zmienione", "Ok");
                        await Navigation.PopAsync();
                    }
                }
                else
                {
                    await DisplayAlert("Błąd", "Wpisane nowe hasła nie są takie same lub są puste", "Ok");
                }
            }
            else
            {
                await DisplayAlert("Błąd", "Poprzednie hasło jest niepoprawne", "Ok");
            }
        }
    }
}