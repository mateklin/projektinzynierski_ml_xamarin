﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinWarsztatClient.Models;
using XamarinWarsztatClient.ViewModels;

namespace XamarinWarsztatClient.Views
{


    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActiveVisit : ContentPage
    {
        private ModelVisit _activeVisit;
        public ActiveVisit(bool archives)
        {
            InitializeComponent();

            var vm = BindingContext as VMMyVisits;
            vm.getAllClientVisits(archives);
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new VisitPage(_activeVisit));
        }

        private void LVCars_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var vm = BindingContext as VMMyVisits;
            var visit = e.Item as ModelVisit;
            _activeVisit = visit;

            vm.HideOrShowVisit(visit);
        }
    }
}