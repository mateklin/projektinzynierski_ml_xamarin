﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinWarsztatClient.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserAccountPage : ContentPage
    {
        public UserAccountPage()
        {
            InitializeComponent();

            eName.Text = Context.Firstname;
            eLastName.Text = Context.Surname;
            eLogin.Text = Context.Login;
            ePesel.Text = Context.Pesel;
            eTown.Text = Context.City;
        }

        private async void ToolbarItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PasswordChangePage());
        }

    }
}