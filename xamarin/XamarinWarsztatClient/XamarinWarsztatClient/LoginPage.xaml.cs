﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XamarinWarsztatClient.Models;
using XamarinWarsztatClient.Views;

namespace XamarinWarsztatClient
{
    public partial class MainPage : TabbedPage
    {
        public MainPage()
        {
            InitializeComponent();
            
        }

        /// <summary>
        /// Zdarzenie wywołane po kliknieciu przycisku zaloguj.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Button_Clicked(object sender, EventArgs e)
        {
            SetControlsEnabled(false);
            this.busyView.IsVisible = true;

            string login, password;
            login = eLogin.Text;
            password = ePassword.Text;

            if(await DataBase.DataBase.CheckUserPassword(login, password, this))
            {
                await DisplayAlert("Alert", "Sukces", "Ok");

                await Navigation.PushModalAsync(new XamarinWarsztatClient.Views.MainPage());
            }
            else
            {
                await DisplayAlert("Alert", "Nie poprawny login bądź hasło", "Ok");
            }

            this.busyView.IsVisible = false;
            SetControlsEnabled(true);
        }

        /// <summary>
        /// Ustawia dostępność kontrolek na ekranie logowania.
        /// </summary>
        /// <param name="enabled">Dostępność kontrolek - True - dostępne, False - niedostepne</param>
        private void SetControlsEnabled(bool enabled)
        {
            eLogin.IsEnabled = enabled;
            ePassword.IsEnabled = enabled;
            BtnLogin.IsEnabled = enabled;
        }

        /// <summary>
        /// Ustawia kontekst aplikacji dla użytkownika po zalogowaniu.
        /// </summary>
        /// <param name="client">Obiekt kliencki dla którego ustawić kontekst</param>
        public async void SetContext(KL_KLIENCI client)
        {
            Context.Firstname = client.KL_IMIE;
            Context.Surname = client.KL_NAZWISKO;
            Context.Login = client.KL_LOGIN;
            Context.City = client.KL_ADRES;
            Context.Pesel = client.KL_PESEL;
            Context.UserId = client.id;

        }

        private async void BtnRegister_Clicked(object sender, EventArgs e)
        {
            if (await DataBase.DataBase.LoginExist(rLogin.Text))
            {
                if (await DataBase.DataBase.AddUser(rLogin.Text, rPassword.Text, rName.Text, rSurname.Text, rPesel.Text, rTown.Text))
                    await DisplayAlert("Alert", "Zarejestrowano pomyślnie.", "OK");
                else
                    await DisplayAlert("Alert", "Błąd podczas rejestracji", "OK");
            }
            else
            {
                await DisplayAlert("Alert", "Podany login jest zajęty!", "OK");
            }
            


        }
    }
}
