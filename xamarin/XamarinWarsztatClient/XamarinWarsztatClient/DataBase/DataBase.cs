﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using XamarinWarsztatClient.Models;

namespace XamarinWarsztatClient.DataBase
{
    /// <summary>
    /// Klasa obsługująca połączenie do bazy danych.
    /// </summary>
    public static class DataBase
    {
        public static async Task<bool> CheckUserPassword(string login, string pass, MainPage mainPage)
        {
            KL_KLIENCI client =  await CheckUser(login, pass);
            if (client != null && client.id != 0)
            {
                mainPage.SetContext(client);
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Metoda sprawdzająca czy istnieje użytkownik o podanym loginie i haśle.
        /// Zwraca obiekt z użytkownikiem (pusty jeśli nie istnieje)
        /// </summary>
        /// <param name="login">Login użytkownika, którego znaleźć</param>
        /// <param name="pass">Hasło użytkownika, którego znaleźć</param>
        /// <returns></returns>
        public static async Task<KL_KLIENCI> CheckUser(string login, string pass)
        {
            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("login", login),
                new KeyValuePair<string, string>("pass", pass)
            });

            string url = "http://xamarinml.nazwa.pl/login.php";
            var client = new HttpClient();
            KL_KLIENCI result = new KL_KLIENCI();

            try
            { 
                var response = await client.PostAsync(url, formContent);

                var json = await response.Content.ReadAsStringAsync();
                result = JsonConvert.DeserializeObject<KL_KLIENCI>(json);
            }
            catch(Exception exp)
            {

            }
        
            return result;
        }

        /// <summary>
        /// Metoda pobierająca wszystkie marki pojazdów z bazy danych.
        /// </summary>
        /// <returns>Listę marek</returns>
        public static async Task<List<BrandCars>> GetAllBrands()
        {
            string url = "http://xamarinml.nazwa.pl/BrandsCars.php";

            var client = new HttpClient();

            List<BrandCars> result = new List<BrandCars>();
            try
            {
                var response = await client.GetAsync(url);
                var json = await response.Content.ReadAsStringAsync();

                result = JsonConvert.DeserializeObject<List<BrandCars>>(json);
            }
            catch(Exception exp)
            {

            }

            return result;
        }
        
        /// <summary>
        /// Metoda pobierająca wszystkie modele pojazdów z bazy danych.
        /// </summary>
        /// <returns>Listę modeli</returns>
        public static async Task<List<ModelsForCars>> GetAllModels()
        {
            string url = "http://xamarinml.nazwa.pl/ModelsCars.php";

            var client = new HttpClient();

            List<ModelsForCars> result = new List<ModelsForCars>();
            try
            {
                var response = await client.GetAsync(url);
                var json = await response.Content.ReadAsStringAsync();

                result = JsonConvert.DeserializeObject<List<ModelsForCars>>(json);
            }
            catch(Exception exp)
            {

            }

            return result;
        }

        /// <summary>
        /// Metoda zapisująca pojazd do bazy
        /// </summary>
        /// <param name="newCar">Obiekt z modelem pojazdu</param>
        /// <returns>True jeżeli uda się zapisać do bazy w przeciwnym wypadku False</returns>
        public static async Task<bool> SaveCar(ModelCars newCar)
        {
            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("userID", newCar.IdKlient.ToString()),
                new KeyValuePair<string, string>("BrandID", newCar.PoMarka.ToString()),
                new KeyValuePair<string, string>("ModelID", newCar.PoModel.ToString()),
                new KeyValuePair<string, string>("vin", newCar.PoVin),
                new KeyValuePair<string, string>("LicenPlate", newCar.PoNrRej),
                new KeyValuePair<string, string>("visible", newCar.PoCzyWidoczny.ToString()),
                new KeyValuePair<string, string>("id", newCar.Id.ToString()),
            });

            string url = "http://xamarinml.nazwa.pl/InsertOrUpdateCar.php";

            var client = new HttpClient();

            bool result = false;
            try
            {
                var response = await client.PostAsync(url, formContent);

                var content = await response.Content.ReadAsStringAsync();

                if (string.IsNullOrWhiteSpace(content))
                    result = true;
            }
            catch (Exception exp)
            {

            }

            return result;
        }

        /// <summary>
        /// Metoda pobierająca wszystkie pojazdy podanego użytkownika
        /// </summary>
        /// <param name="userId">Id użytkownika, którego pojazdy zostana pobrane z bazy</param>
        /// <returns>Listę pojazdów użytkownika</returns>
        public static async Task<List<ModelCars>> GetAllUserCars(int userId)
        {
            List<ModelCars> cars = new List<ModelCars>();

            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("userID", userId.ToString()), 
            });

            try
            {
                string url = "http://xamarinml.nazwa.pl/myCars.php";

                var client = new HttpClient();

                var response = await client.PostAsync(url, formContent);
                var json = await response.Content.ReadAsStringAsync();

                if (!string.IsNullOrWhiteSpace(json))
                    cars = JsonConvert.DeserializeObject<List<ModelCars>>(json);
            }
            catch(Exception exp)
            {

            }

            return cars;
        }

        /// <summary>
        /// Metoda pobierająca wszystkie wizyty użytkownika
        /// </summary>
        /// <param name="userId">Id użytkownika, którego wizyty zostaną pobrane z bazy</param>
        /// <returns>Listę wizyt</returns>
        public static async Task<List<ModelVisit>>  GetAllVisit(int userId, bool archives = false)
        {
            List<ModelVisit> visits = new List<ModelVisit>();

            var formContent = new FormUrlEncodedContent(
                new[]
                {
                    new KeyValuePair<string, string>("userID", userId.ToString()),
                    new KeyValuePair<string, string>("archives", archives ? "1" : "0"),
                });

            try
            {
                string url = "http://xamarinml.nazwa.pl/MyVisits.php";
                var client = new HttpClient();

                var response = await client.PostAsync(url, formContent);
                var json = await response.Content.ReadAsStringAsync();

                if (!string.IsNullOrWhiteSpace(json))
                    visits = JsonConvert.DeserializeObject<List<ModelVisit>>(json); 
            }
            catch(Exception exp)
            {

            }

            return visits;
        }

        /// <summary>
        /// Pobiera listę czynności danej wizyty z bazy danych.
        /// </summary>
        /// <param name="visitId">Id wizyty dla której pobrać czynności</param>
        /// <returns>Listę czynności dla danej wizyty</returns>
        public static async Task<List<ModelService>> GetAllServiceList(int visitId)
        {
            List<ModelService> services = new List<ModelService>();

            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("visitID", visitId.ToString())
            });

            try
            {
                string url = "http://xamarinml.nazwa.pl/Services.php";

                var client = new HttpClient();

                var response = await client.PostAsync(url, formContent);
                var json = await response.Content.ReadAsStringAsync();

                if (!string.IsNullOrWhiteSpace(json))
                    services = JsonConvert.DeserializeObject<List<ModelService>>(json);
            }
            catch (Exception exp)
            {

            }
            return services;
        }

        /// <summary>
        /// Metoda sprawdzająca czy podane hasło dla użytkownika o podanym id jest prawidłowe.
        /// </summary>
        /// <param name="password">Hasło do sprawdzenia</param>
        /// <param name="id">Id użytkownika, którego hasło będzie sprawdzane</param>
        /// <returns>True, jeżeli hasło jest prawidłowe w przeciwnym wypadku False</returns>
        public static async Task<bool> IsPasswordCorrect(string password, int id)
        {
            bool isCorrect = false;

            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("password", password),
                new KeyValuePair<string, string>("userID", id.ToString())
            });

            try
            {
                string url = "http://xamarinml.nazwa.pl/PasswordCorrect.php";
                var client = new HttpClient();

                var response = await client.PostAsync(url, formContent);
                var content = await response.Content.ReadAsStringAsync();
                if (string.IsNullOrWhiteSpace(content))
                    isCorrect = true;
                
            }
            catch
            {

            }

            return isCorrect;
        }

        /// <summary>
        /// Metoda zmieniająca hasło użytkownika.
        /// </summary>
        /// <param name="newPassword">Nowe hasło</param>
        /// <param name="id">Id użytkownika, któremu hasło będzie zmianiane</param>
        /// <returns>True jeżeli uda się zmienic hasło w przeciwnym przypadku Falsewinsc</returns>
        public static async Task<bool> ChangeUserPassword(string newPassword, int id)
        {
            bool result = false;

            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("password", newPassword),
                new KeyValuePair<string, string>("userID", id.ToString())
            });

            try
            {
                string url = "http://xamarinml.nazwa.pl/UpdatePassword.php";
                var client = new HttpClient();

                var response = await client.PostAsync(url, formContent);
                var content = await response.Content.ReadAsStringAsync();
                if (string.IsNullOrWhiteSpace(content))
                    result = true;

            }
            catch
            {

            }

            return result;
        }

        /// <summary>
        /// Metoda sprawdzająca czy podany login widnieje w bazie
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public static async Task<bool> LoginExist(string login)
        {
            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("login", login),
            });

            string url = "http://xamarinml.nazwa.pl/loginexists.php";
            var client = new HttpClient();
            bool result = false;
            try
            {
                var response = await client.PostAsync(url, formContent);

                var content = await response.Content.ReadAsStringAsync();
                if (string.IsNullOrWhiteSpace(content))
                    result = true;
            }
            catch (Exception exp)
            {

            }

            return result;
        }

        /// <summary>
        /// Metoda dodająca użytkownika do bazy
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <param name="name"></param>
        /// <param name="surname"></param>
        /// <param name="pesel"></param>
        /// <param name="town"></param>
        /// <returns></returns>
        public static async Task<bool> AddUser(string login, string password, string name, string surname, string pesel, string town)
        {
            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("login", login),
                new KeyValuePair<string, string>("password", password),
                new KeyValuePair<string, string>("name", name),
                new KeyValuePair<string, string>("surname", surname),
                new KeyValuePair<string, string>("pesel", pesel),
                new KeyValuePair<string, string>("town", town),
            });

            string url = "http://xamarinml.nazwa.pl/addUser.php";
            var client = new HttpClient();
            bool result = false;
            try
            {
                var response = await client.PostAsync(url, formContent);

                var content = await response.Content.ReadAsStringAsync();
                if (string.IsNullOrWhiteSpace(content))
                    result = true;
            }
            catch (Exception exp)
            {

            }

            return result;
        }

    }
}
