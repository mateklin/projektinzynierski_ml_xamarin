﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinWarsztatClient.Models
{
    public class ModelCars
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("PO_MARKA")]
        public long PoMarka { get; set; }

        [JsonProperty("PO_MODEL")]
        public long PoModel { get; set; }

        [JsonProperty("PO_VIN")]
        public string PoVin { get; set; }

        [JsonProperty("ID_KLIENT")]
        public long IdKlient { get; set; }

        [JsonProperty("PO_NR_REJ")]
        public string PoNrRej { get; set; }

        [JsonProperty("PO_CZY_WIDOCZNY")]
        public long PoCzyWidoczny { get; set; }

        [JsonProperty("M_NAZWA")]
        public string MNazwa { get; set; }

        [JsonProperty("MO_NAZWA")]
        public string MoNazwa { get; set; }

        public string CarName { get { return string.Format("{0} {1} - {2}", MNazwa, MoNazwa, PoNrRej); } }

        public bool IsVisible { get; set; }
    }
}
