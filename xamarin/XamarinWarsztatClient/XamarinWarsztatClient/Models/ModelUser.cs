﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinWarsztatClient.Models
{
    public class ModelUser : INotifyPropertyChanged
    {
        private string _login, _password;
        private bool _isBusy;

        private string _rlogin, _rpassword, _name, _surname, _town, _pesel, _repassword;

        public string Login
        {
            get { return _login; }
            set
            {
                if (value != _login)
                {
                    _login = value;
                    OnPropertyChanged("Login");
                    OnPropertyChanged("HasValidInput");
                }
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                if(value != _password)
                {
                    _password = value;
                    OnPropertyChanged("Password");
                    OnPropertyChanged("HasValidInput");
                }
            }
        }

        public bool BusyViewV
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }


        

        public string RRepassword
        {
            get { return _repassword; }
            set
            {
                if (value != _repassword)
                {
                    _repassword = value;
                    OnPropertyChanged("RRepassword");
                    OnPropertyChanged("RHasValidInput");
                }
            }
        }

        public string RLogin
        {
            get { return _rlogin; }
            set
            {
                if (value != _rlogin)
                {
                    _rlogin = value;
                    OnPropertyChanged("RLogin");
                    OnPropertyChanged("RHasValidInput");
                }
            }
        }
        public string RPesel
        {
            get { return _pesel; }
            set
            {
                if (value != _pesel)
                {
                    _pesel = value;
                    OnPropertyChanged("RPesel");
                    OnPropertyChanged("RHasValidInput");
                }
            }
        }


        public string RPassword
        {
            get { return _rpassword; }
            set
            {
                if (value != _rpassword)
                {
                    _rpassword = value;
                    OnPropertyChanged("RPassword");
                    OnPropertyChanged("RHasValidInput");
                }
            }
        }

        public string RName
        {
            get { return _name; }
            set
            {
                if (value != _name)
                {
                    _name = value;
                    OnPropertyChanged("RName");
                    OnPropertyChanged("RHasValidInput");
                }
            }
        }

        public string RSurname
        {
            get { return _surname; }
            set
            {
                if (value != _surname)
                {
                    _surname = value;
                    OnPropertyChanged("RSurname");
                    OnPropertyChanged("RHasValidInput");
                }
            }
        }

        public string RTown
        {
            get { return _town; }
            set
            {
                if (value != _town)
                {
                    _town = value;
                    OnPropertyChanged("RTown");
                    OnPropertyChanged("RHasValidInput");
                }
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
