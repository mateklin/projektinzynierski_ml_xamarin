﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinWarsztatClient.Models
{
    public class ModelVisit
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("W_CENA")]
        public string WCena { get; set; }

        [JsonProperty("W_DATA_PRZY")]
        public string WDataPrzy { get; set; }

        [JsonProperty("W_DATA_ODD")]
        public string WDataOdd { get; set; }

        [JsonProperty("opis")]
        public string Opis { get; set; }

        [JsonProperty("M_NAZWA")]
        public string MNazwa { get; set; }

        [JsonProperty("MO_NAZWA")]
        public string MoNazwa { get; set; }

        [JsonProperty("ilosc_czynnosci")]
        public long IloscCzynnosci { get; set; }

        [JsonProperty("ilosc_wykonanych_czynnosci")]
        public long IloscWykonanychCzynnosci { get; set; }

        public bool IsVisible { get; set; }

        public string VisitName { get
            {
                return string.Format("Wizyta w dn. {0}. Pojazd - {1} {2}. Wykonanie: {3}/{4}", WDataPrzy, MNazwa, MoNazwa, 
                    IloscWykonanychCzynnosci.ToString(), IloscCzynnosci.ToString());
            } }


    }
}
