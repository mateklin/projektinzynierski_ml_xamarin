﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinWarsztatClient.Models
{
    public class BrandCars
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("M_NAZWA")]
        public string MNazwa { get; set; }
    }
}
