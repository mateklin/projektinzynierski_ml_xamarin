﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamarinWarsztatClient.Models
{
    public class ModelService
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("CZ_CZYNNOSC")]
        public string CzCzynnosc { get; set; }

        [JsonProperty("CZ_OPIS")]
        public string CzOpis { get; set; }

        [JsonProperty("CZ_WYKON")]
        public bool CzWykon { get; set; }

        [JsonProperty("CZ_WYKON_PRAC")]
        public object CzWykonPrac { get; set; }

        [JsonProperty("ID_WIZYTY")]
        public long IdWizyty { get; set; }

        public bool IsVisible { get; set; }

        public Color TextColor
        {
            get
            {
                if (CzWykon)
                    return Color.Green;
                else
                    return Color.OrangeRed;
            }
            
        }

        public string CheckedOrUnchecked
        {
            get
            {
                if (CzWykon)
                    return Constants.iconCheckedName;
                else
                    return Constants.iconUncheckedName;
            }
        }
    }
}
