﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinWarsztatClient.Models
{
    public  class KL_KLIENCI
    {
        public int id { get; set; }
        public string KL_IMIE { get; set; }
        public string KL_NAZWISKO { get; set; }
        public string KL_PESEL { get; set; }
        public string KL_ADRES { get; set; }
        public string KL_LOGIN { get; set; }
        public string KL_HASLO { get; set; }
    }
}
