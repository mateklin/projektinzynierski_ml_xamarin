﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinWarsztatClient.Models
{
    public class ModelsForCars
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("ID_MARKI")]
        public long IdMarki { get; set; }

        [JsonProperty("MO_NAZWA")]
        public string MoNazwa { get; set; } 
    }
}
